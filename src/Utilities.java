import java.util.ArrayList;
import java.util.List;

public class Utilities {
	private static final char MONSTER = '*';
	private static final char GOLD = '$';
	private static final char HOLE = '0';

	private static final int MONSTER_VALUE = 50;
	private static final int MONSTER_AREA_VALUE = 10;
	private static final int GOLD_VALUE = -9000;
	private static final int GOLD_AREA_VALUE = -6000;
	private static final int GOLD_AREA_VALUE_2 = -2500;
	private static final int GOLD_AREA_VALUE_3 = -2000;
	private static final int GOLD_AREA_VALUE_4 = -1000;
	private static final int GOLD_AREA_VALUE_5 = -500;


	private static final int HOLE_VALUE = 9000;
	private static final int HOLE_AREA_VALUE = 1000;

	public static void printPlayBoard(char[][] playBoard, int rows, int cols){
		for(int i = 0; i < rows; i++){
			for(int j = 0; j < cols; j++){
				System.out.print(playBoard[i][j]);
				System.out.print("|");
			}
			System.out.println("");
			for(int j = 0; j < cols; j++){
				System.out.print("- ");
			}
			System.out.println("");
		}
		System.out.println("");
	}
	public static String toStringPlayBoard(char[][] playBoard, int rows, int cols){
		String b = "";
		for(int i = 0; i < rows; i++){
			for(int j = 0; j < cols; j++){
				if(j==0)b+= " || "+playBoard[i][j] + " | ";
				else if(j < cols - 1) b+= playBoard[i][j] + " | ";
				else b+= playBoard[i][j] + " || ";
				//b+="|";
			}
			b+="\n";
			for(int j = 0; j < cols; j++){
				//b+="====";
			}
			b+="\n";
		}
		return b;
	}
	
	

	public static void printPlayBoardWArcher(char[][] playBoard, int rows, int cols, int[] archerPos){

	}

	public static void play(char[][] playBoard,Board board, Archer archer){
		if(!archer.hasArrows())
			updateAfterLastShoot(board, playBoard);
		int[] currentPos = archer.getPosition();
		char countChar = 'a';
		while(!board.hasGold(currentPos[0], currentPos[1])){
			playBoard[currentPos[0]][currentPos[1]] = countChar; 
			if(board.wasVisited(currentPos)){
				System.out.println("Infinity loop");
				return;
			}				
			if(board.getValue(currentPos[0], currentPos[1]) > HOLE_VALUE*2)
				board.markVisited(currentPos);
			board.addToValue(currentPos[0], currentPos[1], HOLE_VALUE);
			direction dir = getTheBestMove(board, currentPos);
			if(dir == null){
				System.out.println("Infinity loop");
				return;
			}
			doMoveArcher(archer, dir);
			currentPos = archer.getPosition();
			
			if(board.hasWupus(currentPos[0], currentPos[1])){
				doShoot(archer, board, playBoard);			
			}
			countChar++;
		}

	}
	
	public static void doShoot(Archer archer, Board board, char[][] playBoard){
		archer.shootArrow();
		board.unMarkWupus(archer.getPosition()[0], archer.getPosition()[1]);
		if(!archer.hasArrows()){
			updateAfterLastShoot(board, playBoard);
		}		
	}


	public static void doMoveArcher(Archer archer, direction dir){
		switch (dir) {
		case LEFT:
			archer.moveLeft();
			break;
		case RIGHT:
			archer.moveRight();
			break;
		case UP:
			archer.moveUp();
			break;
		case DOWN:
			archer.moveDown();
			break;
		default:
			break;
		}
	}

	public static direction getTheBestMove(Board board, int[] position) throws Error{
		int[] areaValues = board.getAreaValues(position[0], position[1]);
		int[] smallerPositions = smallerCostPosition(areaValues);
		int i = 0;
		while(true){
			if(i >= smallerPositions.length) return null;
			switch (smallerPositions[i]) {
			case 0:
				if(board.hasHoleLeft(position)) break;
				return direction.LEFT;
			case 1:
				if(board.hasHoleRight(position)) break;
				return direction.RIGHT;
			case 2:
				if(board.hasHoleUp(position)) break;
				return direction.UP;
			case 3:
				if(board.hasHoleDown(position)) break;
				return direction.DOWN;			
			default:
				return null;
			}
			i++;
		}

	}

	private static int[] smallerCostPosition(int[] values){
		int aux[] = values.clone();
		int positions[] = new int[4];

		for(int j = 0; j < aux.length; j++){
			int smaller = 999999999;
			int position = 0;
			for(int i = 0; i < aux.length; i++){
				if(aux[i] < smaller){
					smaller = aux[i];
					position = i;
				}
			}
			aux[position] = 999999999;
			positions[j] = position;
		}
		return positions;

	}

	public static void updateAfterLastShoot(Board board, char[][] playBoard){
		for(int i = 0; i < board.getRows(); i++){
			for(int j = 0; j < board.getCols(); j++){
				if(board.hasWupus(i, j)){
					board.setValue(i, j, HOLE_VALUE);
					board.addHole(i, j);
					board.unMarkWupus(i, j);
					setAreaValues(board, i, j, HOLE_AREA_VALUE, playBoard);
				}
			}
		}
	}

	public static int[] randomGoodPosition(char[][] playBoard, int rows, int cols){
		int randomI = (int)(Math.random() * (rows));
		int randomJ = (int)(Math.random() * (cols));
		if(playBoard[randomI][randomJ] == MONSTER 
				|| playBoard[randomI][randomJ] == GOLD 
				|| playBoard[randomI][randomJ] == HOLE ){
			return randomGoodPosition(playBoard, rows, cols);
		}else{
			return new int[]{randomI, randomJ};
		}
	}
	
	public static boolean isGoodPosition(char[][] playBoard, int rows, int cols, int i, int j){
		if(i >= rows || i < 0) return false;
		if(j >= cols || j < 0) return false;
		if(playBoard[i][j] == MONSTER 
				|| playBoard[i][j] == GOLD 
				|| playBoard[i][j] == HOLE ){
			return false;
		}
		else{
			return true;
		}
	}

	public static Board parseBoard(char[][] playBoard, int rows, int cols){
		if(rows <=0 || cols <= 0){
			throw new IllegalArgumentException("Nothing to parse");
		}
		Board valueBoard = new Board(rows,cols);
		for(int i = 0; i < rows; i++){
			for(int j = 0; j < cols; j++){
				switch (playBoard[i][j]) {
				case MONSTER:
					valueBoard.addToValue(i, j, MONSTER_VALUE);
					valueBoard.addWumpus(i, j);
					addAreaValues(valueBoard, i, j, MONSTER_AREA_VALUE, playBoard);
					break;
				case GOLD:
					valueBoard.addToValue(i, j, GOLD_VALUE);
					valueBoard.addGold(i, j);
					addGoldAreaValue(valueBoard, i, j, GOLD_AREA_VALUE, playBoard);
					break;
				case HOLE:
					valueBoard.addToValue(i, j, HOLE_VALUE);
					valueBoard.addHole(i, j);
					addAreaValues(valueBoard, i, j, HOLE_AREA_VALUE, playBoard);
					break;
				default:
					break;
				}
			}
		}
		return valueBoard;
	}

	private static void addGoldAreaValue(Board board,int i, int j, int value, char[][] playBoard){
		addAreaValues(board, i, j, value, playBoard);
		//GOLD_2
		if(board.isOnBoard(i+1, j+1)){
			if(playBoard[i+1][j+1] != GOLD && playBoard[i+1][j+1] != HOLE)
				board.addToValue(i+1, j+1, GOLD_AREA_VALUE_2);
		}
		if(board.isOnBoard(i+1, j-1)){
			if(playBoard[i+1][j-1] != GOLD && playBoard[i+1][j-1] != HOLE)
				board.addToValue(i+1, j-1, GOLD_AREA_VALUE_2);
		}
		if(board.isOnBoard(i-1, j+1)){
			if(playBoard[i-1][j+1] != GOLD && playBoard[i-1][j+1] != HOLE)
				board.addToValue(i-1, j+1, GOLD_AREA_VALUE_2);
		}
		if(board.isOnBoard(i-1, j-1)){
			if(playBoard[i-1][j-1] != GOLD && playBoard[i-1][j-1] != HOLE)
				board.addToValue(i-1, j-1, GOLD_AREA_VALUE_2);
		}
		//GOLD_3
		if(board.isOnBoard(i, j+2)){
			if(playBoard[i][j+2] != GOLD && playBoard[i][j+2] != HOLE)
				board.addToValue(i, j+2, GOLD_AREA_VALUE_3);
		}
		if(board.isOnBoard(i+2, j)){
			if(playBoard[i+2][j] != GOLD && playBoard[i+2][j] != HOLE)
				board.addToValue(i+2, j, GOLD_AREA_VALUE_3);
		}
		if(board.isOnBoard(i-2, j)){
			if(playBoard[i-2][j] != GOLD && playBoard[i-2][j] != HOLE)
				board.addToValue(i-2, j, GOLD_AREA_VALUE_3);
		}
		if(board.isOnBoard(i, j-2)){
			if(playBoard[i][j-2] != GOLD && playBoard[i][j-2] != HOLE)
				board.addToValue(i, j-2, GOLD_AREA_VALUE_3);
		}

		// GOLD_4
		if(board.isOnBoard(i, j+3)){
			if(playBoard[i][j+3] != GOLD && playBoard[i][j+3] != HOLE)
				board.addToValue(i, j+3, GOLD_AREA_VALUE_4);
		}
		if(board.isOnBoard(i+3, j)){
			if(playBoard[i+3][j] != GOLD && playBoard[i+3][j] != HOLE)
				board.addToValue(i+3, j, GOLD_AREA_VALUE_4);
		}
		if(board.isOnBoard(i-3, j)){
			if(playBoard[i-3][j] != GOLD && playBoard[i-3][j] != HOLE)
				board.addToValue(i-3, j, GOLD_AREA_VALUE_4);
		}
		if(board.isOnBoard(i, j-3)){
			if(playBoard[i][j-3] != GOLD && playBoard[i][j-3] != HOLE)
				board.addToValue(i, j-3, GOLD_AREA_VALUE_4);
		}
		

	}

	private static void addAreaValues(Board board,int i, int j, int value, char[][] playBoard){
		if(board.isOnBoard(i, j+1)){
			if(playBoard[i][j+1] != GOLD && playBoard[i][j+1] != HOLE)
				board.addToValue(i, j+1, value);
		}
		if(board.isOnBoard(i+1, j)){
			if(playBoard[i+1][j] != GOLD && playBoard[i+1][j] != HOLE)
				board.addToValue(i+1, j, value);
		}
		if(board.isOnBoard(i-1, j)){
			if(playBoard[i-1][j] != GOLD && playBoard[i-1][j] != HOLE)
				board.addToValue(i-1, j, value);
		}
		if(board.isOnBoard(i, j-1)){
			if(playBoard[i][j-1] != GOLD && playBoard[i][j-1] != HOLE)
				board.addToValue(i, j-1, value);
		}
	}

	private static void setAreaValues(Board board,int i, int j, int value, char[][] playBoard){
		if(board.isOnBoard(i, j+1)){
			if(playBoard[i][j+1] != GOLD && playBoard[i][j+1] != HOLE){
				if(board.getValue(i, j+1) < value){
					board.setValue(i, j+1, value);
				}else{
					board.addToValue(i, j+1, value);
				}
			}				
		}
		if(board.isOnBoard(i+1, j)){
			if(playBoard[i+1][j] != GOLD && playBoard[i+1][j] != HOLE){
				if(board.getValue(i+1, j) < value){
					board.setValue(i+1, j, value);
				}else{
					board.addToValue(i+1, j, value);
				}
			}
		}
		if(board.isOnBoard(i-1, j)){
			if(playBoard[i-1][j] != GOLD && playBoard[i-1][j] != HOLE){
				if(board.getValue(i-1, j) < value){
					board.setValue(i-1, j, value);
				}else{
					board.addToValue(i-1, j, value);
				}
			}
		}
		if(board.isOnBoard(i, j-1)){
			if(playBoard[i][j-1] != GOLD && playBoard[i][j-1] != HOLE){
				if(board.getValue(i, j-1) < value){
					board.setValue(i, j-1, value);
				}else{
					board.addToValue(i, j-1, value);
				}
			}
		}
	}
	
	public static void chargeMaps(List<char[][]> playBoardsList, List<int[]> playBoardsListSizes){
		playBoardsListSizes.add(new int[]{4,4});
		playBoardsList.add(new char[][]{
			{' ',' ','0','*'},
			{' ','*','$','*'},
			{' ',' ','*','*'},
			{' ',' ',' ',' '}
		});
		playBoardsListSizes.add(new int[]{4,4});
		playBoardsList.add(new char[][]{
			{' ','0','0','*'},
			{' ','0',' ','*'},
			{' ','0','*','*'},
			{'$','0',' ',' '}
		});
		playBoardsListSizes.add(new int[]{4,4});
		playBoardsList.add(new char[][]{
			{'0',' ',' ','*'},
			{'0','0','0',' '},
			{' ',' ',' ',' '},
			{'$',' ',' ',' '}
		});
		
		playBoardsListSizes.add(new int[]{5,4});
		playBoardsList.add(new char[][]{
			{'0',' ','0','*'},
			{'0','*','$','*'},
			{'0',' ','*','*'},
			{'0',' ',' ',' '},
			{'0','0','0',' '}
		});
		
		playBoardsListSizes.add(new int[]{5,4});
		playBoardsList.add(new char[][]{
			{'0',' ',' ','0'},
			{'0',' ','$',' '},
			{'0',' ',' ',' '},
			{'0',' ',' ',' '},
			{'0','0','0','0'}
		});
		
		playBoardsListSizes.add(new int[]{5,5});
		playBoardsList.add(new char[][]{
			{'0','0',' ','*','$'},
			{' ','0',' ','*',' '},
			{'0','0','0',' ',' '},
			{' ',' ','0',' ',' '},
			{' ',' ','0','*',' '}
		});	
		
		playBoardsListSizes.add(new int[]{7, 7});
		playBoardsList.add(new char[][]{
			{'0',' ',' ',' ','0',' ',' '},
			{' ',' ',' ','0',' ',' ',' '},
			{' ',' ',' ',' ',' ',' ',' '},
			{' ',' ',' ',' ',' ','0',' '},
			{'0',' ',' ',' ','*','$',' '},
			{' ',' ',' ',' ',' ',' ',' '},
			{' ','0',' ',' ',' ',' ',' '}
		});	
	}
	
	public static String[] toStringMaps(List<char[][]> playBoardsList, List<int[]> playBoardsListSizes){
		String[] m = new String[playBoardsList.size()];
		for(int i = 0; i < playBoardsList.size(); i++){
			m[i] = toStringPlayBoard(playBoardsList.get(i),
					playBoardsListSizes.get(i)[0],
					playBoardsListSizes.get(i)[1]);
		}
		return m;
	}


}

enum direction{
	LEFT,RIGHT,UP,DOWN;
}
