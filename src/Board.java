
public class Board {
	private Cell[][] board;
	private int rows;
	private int cols;
	
	public Board(int row, int col){
		this.rows = row;
		this.cols = col;
		this.board = new Cell[row][col];
		for(int i = 0; i < rows; i ++){
			for(int j = 0; j < cols; j++){
				board[i][j] = new Cell();
			}
		}
	}
	
	public boolean isOnBoard(int i, int j){
		if(i < 0 || j < 0){
			return false;
		}
		if(i >= rows || j >= cols){
			return false;
		}
		return true;
	}
	
	public int getValue(int i, int j) throws IllegalArgumentException{
		if(!isOnBoard(i, j)){
			throw new IllegalArgumentException("Out of the board");
		}
		return board[i][j].getValue();		
	}
	
	public void addToValue(int i, int j, int value) throws IllegalArgumentException{
		if(!isOnBoard(i, j)){
			throw new IllegalArgumentException("Out of the board");
		}
		board[i][j].addToValue(value);
	}
	
	public int getRows(){
		return this.rows;
	}
	
	public int getCols(){
		return this.cols;
	}
	
	public String toString(){
		String b = "";
		for(int i = 0; i < this.rows; i++){
			for(int j = 0; j < this.cols; j++){
				b += ""+board[i][j].getValue() + "      ";
			}
			b += "\n";
		}
		return b;
	}
	
	public String toStringWumpus(){
		String b = "";
		for(int i = 0; i < this.rows; i++){
			for(int j = 0; j < this.cols; j++){
				b += ""+board[i][j].hasWumpus() + "      ";
			}
			b += "\n";
		}
		return b;		
	}
	public String toStringVisited(){
		String b = "";
		for(int i = 0; i < this.rows; i++){
			for(int j = 0; j < this.cols; j++){
				b += ""+board[i][j].isVisited() + "      ";
			}
			b += "\n";
		}
		return b;		
	}
	public String toStringHoles(){
		String b = "";
		for(int i = 0; i < this.rows; i++){
			for(int j = 0; j < this.cols; j++){
				b += ""+board[i][j].hasHole() + "      ";
			}
			b += "\n";
		}
		return b;		
	}
	
	
	public void addWumpus(int i, int j) throws IllegalArgumentException{
		if(!isOnBoard(i, j)){
			throw new IllegalArgumentException("Out of the board");
		}
		board[i][j].markWumpus();
	}
	public void addGold(int i, int j) throws IllegalArgumentException{
		if(!isOnBoard(i, j)){
			throw new IllegalArgumentException("Out of the board");
		}
		board[i][j].markGold();
	}
	public void addHole(int i, int j) throws IllegalArgumentException{
		if(!isOnBoard(i, j)){
			throw new IllegalArgumentException("Out of the board");
		}
		board[i][j].markHole();
	}
	
	
	public boolean hasWupus(int i, int j) throws IllegalArgumentException{
		if(!isOnBoard(i, j)){
			throw new IllegalArgumentException("Out of the board");
		}
		return board[i][j].hasWumpus();
	}
	
	public void unMarkWupus(int i, int j) throws IllegalArgumentException{
		if(!isOnBoard(i, j)){
			throw new IllegalArgumentException("Out of the board");
		}
		board[i][j].unMarkWumpus();
	}
	
	public boolean hasGold(int i, int j) throws IllegalArgumentException{
		if(!isOnBoard(i, j)){
			throw new IllegalArgumentException("Out of the board");
		}
		return board[i][j].hasGold();
	}
	
	public boolean hasHole(int i, int j) throws IllegalArgumentException{
		if(!isOnBoard(i, j)){
			throw new IllegalArgumentException("Out of the board");
		}
		return board[i][j].hasHole();
	}
	
	public void setValue(int i, int j, int value) throws IllegalArgumentException{
		if(!isOnBoard(i, j)){
			throw new IllegalArgumentException("Out of the board");
		}
		board[i][j].setValue(value);
	}
	
	/**
	 * return the area values
	 * [0]: LEFT
	 * [1]: RIGHT
	 * [2]: UP
	 * [3]: DOWN
	 * if in the border, value = 1000000
	 * @param i
	 * @param j
	 * @return
	 */
	public int[] getAreaValues(int i, int j)throws IllegalArgumentException{
		if(!isOnBoard(i, j)){
			throw new IllegalArgumentException("Out of the board");
		}
		int offBoard = 1000000;
		int[] areaValues = new int[4];
		if(!isOnBoard(i, j-1)){
			areaValues[0] = offBoard;
		}else{
			areaValues[0] = getValue(i, j-1);
		}		
		
		if(!isOnBoard(i, j+1)){
			areaValues[1] = offBoard;
		}else{
			areaValues[1] = getValue(i, j+1);
		}
		
		if(!isOnBoard(i-1, j)){
			areaValues[2] = offBoard;
		}else{
			areaValues[2] = getValue(i-1, j);
		}
		
		if(!isOnBoard(i+1, j)){
			areaValues[3] = offBoard;
		}else{
			areaValues[3] = getValue(i+1, j);
		}
		return areaValues;		
	}
	
	public boolean hasHoleLeft(int[] pos){
		if(!isOnBoard(pos[0], pos[1]-1)){
			return true;
		}
		return board[pos[0]][pos[1]-1].hasHole();
	}
	
	public boolean hasHoleRight(int[] pos){
		if(!isOnBoard(pos[0], pos[1]+1)){
			return true;
		}
		return board[pos[0]][pos[1]+1].hasHole();
	}
	
	public boolean hasHoleUp(int[] pos){
		if(!isOnBoard(pos[0]-1, pos[1])){
			return true;
		}
		return board[pos[0]-1][pos[1]].hasHole();
	}
	
	public boolean hasHoleDown(int[] pos){
		if(!isOnBoard(pos[0]+1, pos[1])){
			return true;
		}
		return board[pos[0]+1][pos[1]].hasHole();
	}
	
	public void markVisited(int[] pos)throws IllegalArgumentException{		
		if(!isOnBoard(pos[0], pos[1])){
			throw new IllegalArgumentException("Out of the board");
		}
		board[pos[0]][pos[1]].markVisited();
	}
	
	public boolean wasVisited(int[] pos)throws IllegalArgumentException{		
		if(!isOnBoard(pos[0], pos[1])){
			throw new IllegalArgumentException("Out of the board");
		}
		return board[pos[0]][pos[1]].isVisited();
	}
	
}
