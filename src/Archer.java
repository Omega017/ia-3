import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Archer {
	private int arrows;
	private int[] position = new int[2];
	private int[] initialPosition = new int[2];
	List<int[]> steps = new ArrayList<int[]>();
	
	public Archer(int arrows, int[] position){
		this.arrows = arrows;
		this.position[0] = position[0];
		this.position[1] = position[1];
		this.initialPosition[0] = position[0];
		this.initialPosition[1] = position[1];
		
	}
	public void changePosition(int i, int j){
		this.position[0] = i;
		this.position[1] = j;
	}
	public void addSteps(int i, int j){
		for(int ii = 1; ii <= Math.abs(i); ii++){
			if(i > 0){
				steps.add(new int[] {this.position[0]+ii,this.position[1]});
			}
			else{
				steps.add(new int[] {this.position[0]-ii,this.position[1]});
			}
		}
		for(int jj = 1; jj <=  Math.abs(j); jj++){
			if(j > 0){
				steps.add(new int[] {this.position[0],this.position[1]+jj});
			}
			else{
				steps.add(new int[] {this.position[0],this.position[1]-jj});
			}			
		}
		this.position[0] += i;
		this.position[1] += j;
	}
	public void moveRight(){
		addSteps(0, 1);
	}
	public void moveLeft(){
		addSteps(0, -1);
	}
	public void moveUp(){
		addSteps(-1, 0);
	}
	public void moveDown(){
		addSteps(1, 0);
	}
	public boolean shootArrow(){
		if(this.arrows == 0){
			return false;
		}
		this.arrows--;
		return true;
	}
	public int[] getPosition(){
		return position;
	}
	
	public String toStringPosition(){
		return "[ "+position[0]+", "+position[1]+" ]";
	}
	public String toStringInitialPosition(){
		return "[ "+initialPosition[0]+", "+initialPosition[1]+" ]";
	}
	public String toStringSteps(){
		String b = "";
		for(Iterator<int[]> i = steps.iterator(); i.hasNext(); ) {
		    int[] item = i.next();
		    b+= "["+item[0]+", "+item[1]+"]  ";
		}
		return b;
	}
	
	/**
	 * Includes first position and steps
	 */
	public String toStringCompleteTravel(){
		String b = "["+initialPosition[0]+", "+initialPosition[1]+"]  ";
		for(Iterator<int[]> i = steps.iterator(); i.hasNext(); ) {
		    int[] item = i.next();
		    b+= "["+item[0]+", "+item[1]+"]  ";
		}
		return b;
	}
	
	public boolean hasArrows(){
		if(this.arrows > 0){
			return true;
		}else{
			return false;
		}
	}
}
