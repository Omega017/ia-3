import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
public class Main {
	
	public static void main(String[] args) {
		List<char[][]> playBoardsList = new ArrayList<>();
		List<int[]> playBoardsListSizes = new ArrayList<>();
		Utilities.chargeMaps(playBoardsList, playBoardsListSizes);
		System.out.println("What map do you want to try?");
		String separator = "=========================="
				+ "===============================";
		
		System.out.println(" ");
		for(int i = 0; i < playBoardsList.size(); i++){
			System.out.println(separator+separator);
			System.out.println((i+1)+":\n"+Utilities
					.toStringMaps(playBoardsList, playBoardsListSizes)[i]);			
		}
		
		System.out.println("Answer: ");
		Scanner scanner = new Scanner(System.in);
		String asw = scanner.nextLine();
		while(true){
			if(asw.matches("\\d+")){
				int op = Integer.parseInt(asw);
				if(!(op > playBoardsList.size()) && op != 0) break;
			}
			System.out.println("Choose a correct answer");
			asw = scanner.nextLine();
		}
		int p = Integer.parseInt(asw);
		
		System.out.println("How many arrows should the archer have?:");
		asw = scanner.nextLine();
		while(true){
			if(asw.matches("\\d+")){
				int op = Integer.parseInt(asw);
				if(op > 0) break;
			}
			System.out.println("Have to be a positive integer");
			asw = scanner.nextLine();
		}
		int arrows = Integer.parseInt(asw);
		
		System.out.println("Do you want to choose the start position of the archer?(y/n)");
		asw = scanner.nextLine();
		while(true){
			if(asw.matches("[yn]")){
				break;
			}
			System.out.println("Choose a correct answer");
			asw = scanner.nextLine();
		}
		
		int[] startPosition = new int[2];
		if(asw.matches("n")){
			startPosition = Utilities.randomGoodPosition(
					playBoardsList.get(p-1),
					playBoardsListSizes.get(p-1)[0],
					playBoardsListSizes.get(p-1)[1]
							
			);
		}				
		else{
			System.out.println("i(Row posotion):");
			asw = scanner.nextLine();
			System.out.println("j(Column posotion):");
			String asw2 = scanner.nextLine();
			while(true){
				if(asw.matches("\\d+") && asw2.matches("\\d+")){
					int i = Integer.parseInt(asw);
					int j = Integer.parseInt(asw2);
					if(Utilities.isGoodPosition(playBoardsList.get(p-1), 
							playBoardsListSizes.get(p-1)[0], 
							playBoardsListSizes.get(p-1)[1], 
							i, j)){
						break;
					}
				}
				System.out.println("Choose a correct position "
						+ "(It cannot be on the position of a wumpus, a hole or the gold)");
				System.out.println("i(Row posotion):");
				asw = scanner.nextLine();
				System.out.println("j(Column posotion):");
				asw2 = scanner.nextLine();
			}
			int i = Integer.parseInt(asw);
			int j = Integer.parseInt(asw2);
			startPosition = new int[]{i, j};
		}
		
		Archer archer = new Archer(
				arrows,
				startPosition
		);
		Utilities.play(
				playBoardsList.get(p-1),
				Utilities.parseBoard(
						playBoardsList.get(p-1),
						playBoardsListSizes.get(p-1)[0],
						playBoardsListSizes.get(p-1)[1]
				),
				archer
		);
		System.out.println("\n"+separator+separator);
		System.out.println("The archer begin in "
				+ "("+startPosition[0]+", "+startPosition[1]+")"
				+" with "+ arrows +" arrow(s)");
		System.out.println("Map:(the letters represent the movements of the archer)\n"+Utilities
				.toStringMaps(playBoardsList, playBoardsListSizes)[p-1]);
		System.out.println("Archer steps:\n"+ archer.toStringCompleteTravel());
		scanner.close();	
	}

}
